﻿using UnityEngine;
using System.Collections;

public class Finder : MonoBehaviour {

    public Transform player;

    public Transform[] wayPoints;
    private Transform currentWP;
    private int i;

    public float range;

    public float distanceToPlayer;
    public float distanceToWP;

    private RaycastHit hit;

    private float nextShootTime = 0.0f;
    public float shootPeriod = 2f;
    public GameObject bullet;

    void Awake() {
        currentWP = wayPoints[0];
    }

    void Update () {
        distanceToPlayer = Vector3.Distance(player.position, transform.position);
        distanceToWP = Vector3.Distance(currentWP.position, transform.position);

        if (distanceToWP < 2)
        {
            i++;

            if (i == wayPoints.Length)
            {
                i = 0;
            }

            currentWP = wayPoints[i];
        }

        if (Physics.Linecast(transform.position, player.position, out hit))
        {
            if ((hit.transform.gameObject.name == "Player") && (distanceToPlayer <= range))
            {
                transform.LookAt(player.position);

                transform.GetComponent<NavMeshAgent>().destination = player.position;

                if (Time.time > nextShootTime)
                {
                    shootBullet();

                    nextShootTime += shootPeriod;
                }
            }
            else
            {
                transform.GetComponent<NavMeshAgent>().destination = currentWP.position;
            }
        }
    }

    void shootBullet ()
    {
        Instantiate(bullet, new Vector3(transform.position.x, 0.75f, transform.position.z), transform.rotation);
    }
}
