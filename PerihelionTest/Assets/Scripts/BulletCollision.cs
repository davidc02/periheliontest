﻿using UnityEngine;
using System.Collections;

public class BulletCollision : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        if (other.transform.gameObject.name == "Player")
            Player.lives--;

        Destroy(this.gameObject);
    }
}
