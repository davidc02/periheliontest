﻿using UnityEngine;
using System.Collections;

public class BulletForce : MonoBehaviour {

    private Rigidbody rb;
    public float speed;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }


    void Update () {
        rb.velocity = transform.forward * speed;
    }
}
