﻿using UnityEngine;
using System.Collections;

public class CollisionDetection : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            //melee damage
        }

        if (other.gameObject.tag == "Victory" && Player.isAlive)
        {
            GameConditions.playerWins = true;
        }
    }

}
