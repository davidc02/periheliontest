﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameOverButton : MonoBehaviour {

    public CanvasGroup buttonCG;

    public void EnableGameOverButton()
    {
        buttonCG.interactable = true;
        buttonCG.blocksRaycasts = true;
        buttonCG.alpha = 1;
    }

    public void RestartGame()
    {
        Player.lives = 3;
        Time.timeScale = 1;
        buttonCG.interactable = false;
        buttonCG.blocksRaycasts = false;
        buttonCG.alpha = 0;

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
