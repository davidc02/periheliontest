﻿using UnityEngine;
using System.Collections;

public class GameConditions : MonoBehaviour {

    public GameObject gameOverButton;
    public static bool playerWins;

    void Start ()
    {
        Time.timeScale = 0;
    }

	void Update () {
	    if (Player.lives == 0)
        {
            Time.timeScale = 0;
            Player.isAlive = false;

            gameOverButton.GetComponent<GameOverButton>().EnableGameOverButton();
        }
	}
}
