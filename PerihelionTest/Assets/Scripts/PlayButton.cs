﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayButton : MonoBehaviour {

    public CanvasGroup buttonCG;

    public void PlayButtonAction()
    {
        Time.timeScale = 1;

        buttonCG.interactable = false;
        buttonCG.blocksRaycasts = false;
        buttonCG.alpha = 0;
    }
}
