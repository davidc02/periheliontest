﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VictoryText : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if (GameConditions.playerWins)
        {
            gameObject.GetComponent<Text>().color = new Color(0, 0, 0, 1);
        }
	}
}
